﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Manage_a_Queue
{
    class Program
    {
        static void Main(string[] args)
        {
            char latter;
            for (latter = 'A'; latter <= 'Z'; latter++)
            {
                Console.Write("{0} ", latter);
            }

            //---------------------------------------------------------------------//

            Console.Write("\nPlease enter your specific letter : ");
            string input = Console.ReadLine();
            if (input.Length == 1)
            {
                string inputUpper = input.ToUpper();

                char character = inputUpper.ToCharArray()[0];
                for (latter = 'A'; latter <= character; latter++)
                {
                    Console.Write("{0} ", latter);
                }
            }
            else
            {
                Console.Write("Please enter single character from A to Z");                
            }
            Console.ReadKey();
        }
    }
}

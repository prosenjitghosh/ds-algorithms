﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Quick_Sort_on_an_Array
{
    class Program
    {
        private static void Quick_Sort(int[] arr, int left, int right)
        {
            if (left < right)
            {
                int pivot = Partition(arr, left, right);

                if (pivot > 1)
                {
                    Quick_Sort(arr, left, pivot - 1);
                }
                if (pivot + 1 < right)
                {
                    Quick_Sort(arr, pivot + 1, right);
                }
            }
        }

        private static int Partition(int[] arr, int left, int right)
        {
            int pivot = arr[left];
            while (true)
            {
                while (arr[left] < pivot)
                {
                    left++;
                }

                while (arr[right] > pivot)
                {
                    right--;
                }

                if (left < right)
                {
                    if (arr[left] == arr[right]) return right;

                    int temp = arr[left];
                    arr[left] = arr[right];
                    arr[right] = temp;
                }
                else
                {
                    return right;
                }
            }
        }
        static void Main(string[] args)
        {
            string[] array = { "state1024", "county763", "dma435", "state234", "state100", "county297", "dma4", "dma897" };

            var var_0 = array[0];
            var var_1 = array[1];
            var var_2 = array[2];
            var var_3 = array[3];
            var var_4 = array[4];
            var var_5 = array[5];
            var var_6 = array[6];
            var var_7 = array[7];

            string var_00 = var_0.Remove(0, 5);
            string var_11 = var_1.Remove(0, 6);
            string var_22 = var_2.Remove(0, 3);
            string var_33 = var_3.Remove(0, 5);
            string var_44 = var_4.Remove(0, 5);
            string var_55 = var_5.Remove(0, 6);
            string var_66 = var_6.Remove(0, 3);
            string var_77 = var_7.Remove(0, 3);

            int var_000 = Int32.Parse(var_00);
            int var_111 = Int32.Parse(var_11);
            int var_222 = Int32.Parse(var_22);
            int var_333 = Int32.Parse(var_33);
            int var_444 = Int32.Parse(var_44);
            int var_555 = Int32.Parse(var_55);
            int var_666 = Int32.Parse(var_66);
            int var_777 = Int32.Parse(var_77);

            int[] arr = new int[] { var_000, var_111, var_222, var_333, var_444, var_555, var_666, var_777 };

            //int temp = 0;

            //for (int i = 0; i <= arr.Length - 1; i++)
            //{
            //    for (int j = i + 1; j < arr.Length; j++)
            //    {
            //        if (arr[i] > arr[j])
            //        {
            //            temp = arr[i];
            //            arr[i] = arr[j];
            //            arr[j] = temp;
            //        }
            //    }
            //}

            Console.WriteLine("Original array : ");
            foreach (var item in array)
            {
                Console.Write(" " + item);
            }
            Console.WriteLine();

            Quick_Sort(arr, 0, arr.Length - 1);

            Console.WriteLine("\nSorted array by the numbers in ascending order : ");

            foreach (var item in arr)
            {
                if (item == var_000)
                {
                    Console.Write(" " + var_0);
                }
                if (item == var_111)
                {
                    Console.Write(" " + var_1);
                }
                if (item == var_222)
                {
                    Console.Write(" " + var_2);
                }
                if (item == var_333)
                {
                    Console.Write(" " + var_3);
                }
                if (item == var_444)
                {
                    Console.Write(" " + var_4);
                }
                if (item == var_555)
                {
                    Console.Write(" " + var_5);
                }
                if (item == var_666)
                {
                    Console.Write(" " + var_6);
                }
                if (item == var_777)
                {
                    Console.Write(" " + var_7);
                }
            }
            Console.ReadKey();
        }
    }
}
